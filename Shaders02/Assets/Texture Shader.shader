﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Texture Shader" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_SpecColour("Specular Color", Color) = (1,1,1,1)
		_Shininess("Shininess", float) = 10
	}
	SubShader {
		Pass{

			CGPROGRAM
			#pragma vertex vertexFunction
			#pragma fragment fragmentFunction

			//user defined variables
			uniform float4 _Color;
			uniform float4 _SpecColour;
			uniform float _Shininess;

			//unity defined variables
			uniform float4 _LightColor0;

			//input struct
			struct inputStruct
			{
				float4 vertexPos : POSITION;
				float3 vertexNormal : NORMAL;
			};

			//output struct ?
			struct outputStruct
			{
				float4 pixelPos: SV_POSITION;
				float4 pixelCol : COLOR;
				float3 normalDirection : TEXCOORD0;
				float4 pixelWorldPos : TEXCOORD1;
			};

			//vertex program
			outputStruct vertexFunction(inputStruct input)
			{
				outputStruct toReturn;

				float3 lightDirection;
				float attenuation = 1.0;

				lightDirection = normalize(_WorldSpaceLightPos0.xyz);

				float3 normalDirection = normalize(mul(float4(input.vertexNormal, 0.0), unity_ObjectToWorld).xyz);

				float3 viewDirection = normalize(float3(float4(_WorldSpaceCameraPos.xyz, 1.0) - mul(unity_ObjectToWorld, input.vertexPos).xyz));

				float3 specularReflection = reflect(-lightDirection, normalDirection);

				float3 diffuseReflection = attenuation * _LightColor0.xyz * max(0.0, dot(normalDirection, lightDirection));

				float3 finalLight = specularReflection + diffuseReflection + UNITY_LIGHTMODEL_AMBIENT;

				toReturn.pixelCol = float4(finalLight * _SpecColour, 1.0);

				toReturn.pixelPos = UnityObjectToClipPos(input.vertexPos);

				toReturn.normalDirection = normalDirection;

				toReturn.pixelWorldPos = normalize(float4(_WorldSpaceCameraPos.xyz, 1.0));

				return toReturn;
			}

			//fragment program
			float4 fragmentFunction(outputStruct input) : COLOR
			{
				return input.pixelCol;
			}

			ENDCG
		}
	}
}